
SUMMARY = "Xenomai is a Free Software project in which engineers from a wide background collaborate to build a versatile real-time framework for the Linux© platform. "
HOMEPAGE = "http://www.xenomai.org"
SECTION = "kernel/real-time"

LICENSE = "LGPL"
LIC_FILES_CHKSUM = "file://lib/cobalt/COPYING;md5=68ad62c64cc6c620126241fd429e68fe"

DEPENDS = "hostperl-runtime-native"

SRC_URI = "https://source.denx.de/Xenomai/xenomai/-/archive/v3.2/xenomai-${PV}.tar.bz2"
SRC_URI[md5sum] = "6b0aeb0e016e326a9eb592dfc210d4cd"
SRC_URI[sha256sum] = "0b1da458f0b43eabee240266c5f0ccc3b1899973b838c59e2e5ea46f8467af5a"

SRC_URI += "file://0001-Fix-the-issue-which-expected-next-time-is-less-than-.patch \
file://0002-Fix-the-issue-which-altency-use-legacy-and-wrong-api.patch \
file://0003-Fix-the-issue-which-results-to-bus-error.patch \
file://add-configure-file.patch \
"

inherit lib_package 

B = "${WORKDIR}/build"
do_configure[cleandirs] = "${B}"

EXTRA_OECONF = "--disable-debug --enable-smp ${@bb.utils.contains('XENOMAI_KERNEL_MODE', 'cobalt', '--with-core=cobalt', '--with-core=mercury', d)}"

do_configure () {

	os=${HOST_OS}
	case $os in
	linux-gnueabi |\
	linux-gnuspe |\
	linux-musleabi |\
	linux-muslspe |\
	linux-musl )
		os=linux
		;;
	*)
		;;
	esac
	target="$os-${HOST_ARCH}"
	case $target in
	linux-arm)
		host=arm-poky-linux-gnueabi
		;;
	linux-armeb)
		host=arm-poky-linux-gnueabi
		;;
	linux-aarch64*)
		host=aarch64-poky-linux
		;;		
	esac
	perl ${S}/configure --build=i686-pc-linux-gnu --host=$host ${EXTRA_OECONF}
}

do_install() {
    oe_runmake DESTDIR=${D} install
}

PACKAGE_DEBUG_SPLIT_STYLE = "debug-file-directory"
FILES_${PN} += "/usr"
INSANE_SKIP_${PN} += "dev-so staticdev libdir"
