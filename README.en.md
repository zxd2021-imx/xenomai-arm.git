# Xenomai on i.MX ARMv7 Platform

#### Description
Xenomai is real-time framework, which can run seamlessly side-by-side Linux as a co-kernel system. This a porting for i.MX

#### How to build

copy xenomai-arm to <Yocto folder>/sources/meta-imx/meta-bsp/recipes-kernel, add the following variable in conf/local.conf before build xenomai by command bitake xenomai.

XENOMAI_KERNEL_MODE = "cobalt" 

IMAGE_INSTALL_append += " xenomai"

DISTRO_FEATURES_remove = "optee"

or
XENOMAI_KERNEL_MODE = "mercury"

IMAGE_INSTALL_append += " xenomai"

DISTRO_FEATURES_remove = "optee"

If XENOMAI_KERNEL_MODE = "cobalt", you can build dual kernel version. And If XENOMAI_KERNEL_MODE = "mercury", it is single kernel with PREEMPT-RT patch.